// Use the "require" (built-in function) directive to load the HTTP module of node.js
// Module - software component or part of a program that contains one or more routines
// HTTP Module - lets node.js transfer data using the HTTP (HyperText Trasnfer Protocol)
// HTTP is a protocol that allows the fetching of resources such as HTML documents
// Clients (broswer) and servers (NodeJS/ExpressJS applications) communicates to exchange individual messages
// Request - messages sent by the client (usually in a web broswer)
// Responses - messages sent by the server as an answer to the client

let http = require("http")

// [Section] Create a Server
/*
	- The http module has a createServer() method that accepts a function as an argument and allows the server creation
	- The arguments passed in the functions are request and response objects (data  type) that contain methods that allows us to receive requests from the client and server send the reponses back
	- Using the module's createServer() method, we can create an HTTP server that listens to the request on a specified port and gives back to the client

	- Syntax:
		http.createServer(function(request,response){})
*/


// [Section] Define Port Number
/*
	- A port is a virtual point where network connections start and ends
	- Syntax:
		http.createServer(function(request,response){},listen())
*/


// [Section] Send a Response Back to Client
/*
	- Use the writeHead() method for status code
	- Set a status code for the response: 200 OK
*/
	http.createServer(function(request,response){
		response.writeHead(200,{'Content-Type': 'Text/Plain'});
		response.end('Hello World')
	}).listen(4000)

	console.log('Server running at LocalHost: 4000')

// Inputting the command tells our device to run node.js
	// node index.js

// Install nodemon via terminal
	// npm install -g nodemon

// To check if nodemon is already installed via terminal
	// nodemon -v or nodemon

// Important Note:
/*
	-Installing the package will allow the server to automatically restart when files have been changed or updated.
	- "-g" refers to a global installation where the current version of the package or dependency will be installed locally on our device allowing us to 
*/

